// By Jethzabell Medina

package main

import (
	"fmt"
	"log"
	"net/http"
	"realese-notes/endpoints"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

var e *echo.Echo

func extractTokenMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		var PRIVATE_TOKEN = "PRIVATE-TOKEN"

		if originalAuthorizationHeader := c.Request().Header.Get(PRIVATE_TOKEN); originalAuthorizationHeader != "" {
			c.Set(PRIVATE_TOKEN, originalAuthorizationHeader)
			log.Println("PRIVATE-TOKEN available: ", originalAuthorizationHeader)
			//proceed
			return next(c)
		}
		return echo.NewHTTPError(http.StatusUnauthorized, "Access is denied. User is unauthorized")
	}
}

func main() {
	fmt.Println("Welcome to the server")
	e := echo.New()
	e.Pre(extractTokenMiddleware)
	e.Pre(middleware.RemoveTrailingSlash())

	e.GET("domain/:domain/group/:group", endpoints.GetMergeDescription)
	e.Start(":1111")
}
