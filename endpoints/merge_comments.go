package endpoints

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"realese-notes/models"

	"github.com/labstack/echo"
)

func GetMergeDescription(c echo.Context) error {
	token := c.Request().Header.Get("PRIVATE-TOKEN")
	domain := c.Param("domain")
	group := c.Param("group")
	labels := c.QueryParam("labels")
	created_after := c.QueryParam("created_after")
	per_page := c.QueryParam("per_page")
	if per_page == "" {
		per_page = "100"
	}
	requestURL := "url"
	requestURL = "https://gitlab." + domain + ".com/api/v4/groups/" + group + "/merge_requests?labels=" + labels + "&created_after=" + created_after + "&per_page=" + per_page + "&private_token=" + token

	log.Println(requestURL)
	response, err := http.Get(requestURL)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}
	log.Println(response.Body)
	log.Println(response.StatusCode)
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	// ------------ END REQUEST ------------

	// ------------ UNMARSHALL ------------
	payloads, err1 := MergeDescriptionUnmarshall([]byte(body))
	if err1 != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err1)
	}

	log.Println(payloads)

	theResponse := []models.OurResponse{}

	if payloads == nil {
		theResponse = []models.OurResponse{}
		return c.JSON(http.StatusOK, theResponse)
	}

	for _, payload := range payloads {
		oneResponse := models.OurResponse{
			Author:      payload.Author.Name,
			Description: payload.Description,
			Label:       payload.Labels[0],
			Title:       payload.Title,
		}
		theResponse = append(theResponse, oneResponse)
	}

	return c.JSON(http.StatusOK, theResponse)
}

//GetMergeDescriptions Unmarshal
func MergeDescriptionUnmarshall(body []byte) ([]models.MergeResponse, error) {
	s := []models.MergeResponse{}
	log.Println("unmarshal")
	err := json.Unmarshal(body, &s)
	if err != nil {
		return nil, err
	}
	log.Println(err)
	return s, nil
}

// ------------ CREATING RESPONSE ------------
// theResponse := models.Response{
// 	PlaceName:   s.Features[0].PlaceName,
// 	Temperature: forecast.Currently.Temperature,
// 	Summary:     forecast.Daily.Data[0].Summary,
// }
// // ------------ END CREATING RESPONSE ------------

// return c.JSON(http.StatusOK, theResponse)
