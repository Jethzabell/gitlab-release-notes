package models

type MergeResponse struct {
	Title       string   `json:"title"`
	Description string   `json:"description"`
	Labels      []string `json:"labels"`
	Author      Author   `json:"author"`
}

type Author struct {
	Name string `json:"name"`
}
