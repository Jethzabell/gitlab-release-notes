package models

type OurResponse struct {
	Title       string `json:"title"`
	Description string `json:"description"`
	Label       string `json:"label"`
	Author      string `json:"author"`
}
